var planet, msgcanvas, infocanvas ;
var msgCnt = 20;
var vegId  = undefined , herbId = undefined , extincId = undefined , evoId = undefined , alimId = undefined ; 
var planetCtx;
var cote        = 20 ;
var qSize       = 8 ;
var mpi         = Math.PI*2 ; 
var quartdecote = 1 ;
var cnt         = 0 ;
var running     = false ;
var mirroir  ;
var vitesse     = 1000 ;
var rangeR      = 4 ;
var origX       = 0 ; origY =0 ;
var herbiArr    = new Array() ;
var grassCnt    = 0 ,herbiCnt = 0;
var annee       = 0 ;

//
function displayPlanet() {
	msgcanvas   = document.getElementById('message');
	infocanvas  = document.getElementById('info');
	planet      = document.getElementById('surface');
	planetWidth = planet.width ;
	planetHeight= planet.height ;
	//
	// planet canvas
	planetCtx  = planet.getContext('2d');
	planetCtx.fillStyle     = 'rgba( 150, 102, 2, 1)';
	mirroir = planetCtx.createImageData(planetWidth, planetHeight);
	planetCtx.fillRect( 0 , 0 , 600 , 600 );
	planetCtx.closePath();
	//
	// message canvas
    msgCtx = msgcanvas.getContext('2d');
    msgCtx.clearRect(0, 0, msgcanvas.width, msgcanvas.height);
    msgCtx.font = '10pt Calibri';
    msgCtx.fillStyle = 'black';
    
    // events
	planet.addEventListener('click', function(evt) {
	   var mousePos = getMousePos( planet, evt);
	   origX =  mousePos.x ;
	   origY =  mousePos.y ;
	   var message = 'Planting Vegetal!';
	   writeMessage( message);
	   runLife(  );
	}, false);
	//drawGrid() ;
}

//
function runLife (  ) {
	initiation(  ) ;
	// reproduction
	clearInterval(vegId);
	vegId = setInterval( vegetal, vitesse );
	/*alimentation() ;
	 */
	clearInterval(evoId);
	evoId = setInterval( evolution, vitesse );
	/*mutation() ;*/
	// extinction
	clearInterval(extincId);
	extincId = setInterval( extinction, vitesse*2 );
}

//
function initiation() {
	vitesse = 1000 ;
	rangeR = 4 ;
	xi = origX ; yi = origY ;
	for (i = 0; i < 4; i++) {
	    r = 0 ; g = 64 ; b = 0 ;
	    setPixel( mirroir, xi, yi, 0, 128, 0, 200) ;
	    xi = xi+Math.floor((Math.random() * 20) -9); // from -10 to +10
	    yi = yi+Math.floor((Math.random() * 20) -9);
	}
}

function vegetal() {
//	alimentation() ;
	reproduction() ;
//	mutation() ;
}

function herbivore() {
	alimentation() ;
	reproduction() ;
//	mutation() ;
}

//
function alimentation() {
	xa = herbiArr[0].x ; //(xr-rangeR) + Math.floor(Math.random() * rangeR*2); 
	ya = herbiArr[0].y ; //(yr-rangeR) + Math.floor(Math.random() * rangeR*2);
	setPixel( mirroir, xa+4, ya+4, 250, 250, 250, 254) ;
	setPixel( mirroir, xa+8, ya-8, 250, 250, 250, 254) ;
	setPixel( mirroir, xa-10, ya+10, 250, 250, 250, 254) ;
	setPixel( mirroir, xa-8, ya+4, 250, 250, 250, 254) ;
	//console.log( herbiArr.length ) ;
	//console.log( herbiArr[0].x ) ;
	//console.log( herbiArr.size ) ;
}

//
function reproduction() {
	annee += 0.05 ;
	if ( Math.round(annee*100)%100 == 0 ) writeMessage ( Math.round( annee*10 )/10 + " Million years" ) ;
	xr = origX ; yr = origY ;
	rangeR+= 4 ;
	if ( xr-rangeR < 0 || xr+rangeR > 600 ) {
		origX=origX+Math.floor(Math.random() * origX/4);
		rangeR = 10; vitesse+=1000;
	}
	
	if ( yr-rangeR < 0 || yr+rangeR > 600 ) {
		origY=origY+Math.floor(Math.random() * origY/4);
		rangeR = 10; vitesse+=1000;
	}
	//
	for (i = 0; i < rangeR; i++) {
		xt = (xr-rangeR) + Math.floor(Math.random() * rangeR*2); 
		yt = (yr-rangeR) + Math.floor(Math.random() * rangeR*2);
		
		xi = (xt*4) - (qSize/2*4) ; if ( xi < 0 ) xi = 0 ;
		yi = (yt*4) - (qSize/2*4) ; if ( yi < 0 ) yi = 0 ;
		index =  Math.floor( xi + ( yi * mirroir.width) ) ;
		if ( mirroir.data[index+1] == 128 ) {
			setPixel( mirroir, xt-2, yt+1, 0, 128, 0, 254) ;
			setPixel( mirroir, xt-4, yt-4, 0, 128, 0, 254) ;
			setPixel( mirroir, xt+1, yt+1, 0, 128, 0, 254) ;
			setPixel( mirroir, xt+1, yt-2, 0, 128, 0, 254) ;			
			setPixel( mirroir, xt-2, yt-2, 0, 128, 0, 254) ;			
		}
	}
}

function extinction() {
	xe = origX ; ye = origY ;
	//
	for (i = 0; i < rangeR*2; i++) {
		xe = Math.floor(Math.random() * mirroir.width); 
		ye = Math.floor(Math.random() * mirroir.height);
		
		xei = (xe*4) - (qSize/2*4) ; if ( xei < 0 ) xei = 0 ;
		yei = (ye*4) - (qSize/2*4) ; if ( yei < 0 ) yei = 0 ;
		indexe =  Math.floor( xei + ( yei * mirroir.width) ) ;
		if ( mirroir.data[indexe+1] > 0 ) {
			setPixel( mirroir, xe, ye, 150, 102, 2, 254) ;		
			setPixel( mirroir, xe-1, ye, 150, 102, 2, 254) ;		
			setPixel( mirroir, xe, ye-1, 150, 102, 2, 254) ;		
		}
	}
}
///// 
function evolution() {
	for (i = 0; i < rangeR*4; i++) {
		xv = Math.floor(Math.random() * mirroir.width); 
		yv = Math.floor(Math.random() * mirroir.height);
		
		xvi = (xv*4) - (qSize/2*4) ; if ( xvi < 0 ) xvi = 0 ;
		yvi = (yv*4) - (qSize/2*4) ; if ( yvi < 0 ) yvi = 0 ;
		indexv =  Math.floor( xvi + ( yvi * mirroir.width) ) ;
		if ( mirroir.data[indexv+1] > 0 ) {
			grassCnt++ ;
			if ( grassCnt == 20 ) {
				writeMessage("Evolution: Herbivores" ) ;
				setPixel( mirroir, xv, yv, 250, 250, 250, 254) ;
				herbiArr = [{ 'herbiNum' : 1, 'x' : xv,'y' : yv  }];
				alimId = setInterval( alimentation, vitesse );
			}
		}
	}
}

function drawRandom() {
	// les vegetaux
	for (i = 0; i < 10; i++) {
	    x = Math.random() * planetWidth | 0; // |0 to truncate to Int32
	    y = Math.random() * planetHeight | 0;
	    r = 0;//Math.random() * 256 | 0;
	    g = 216 ;//Math.random() * 256 | 0;
	    b = 0;//Math.random() * 256 | 0;
	    setPixel( mirroir, x, y, r, g, b, 255); // 255 opaque
	    planetCtx.closePath();
	}
	// les herbivores
	for (i = 0; i < 10; i++) {
	    x = Math.random() * planetWidth | 0; // |0 to truncate to Int32
	    y = Math.random() * planetHeight | 0;
	    r = 210;//Math.random() * 256 | 0;
	    g = 216;//Math.random() * 256 | 0;
	    b = 210;//Math.random() * 256 | 0;
	    setPixel( mirroir, x, y, r, g, b, 255); // 255 opaque
	    planetCtx.closePath();
	}
	// les carnivores
	for (i = 0; i < 10; i++) {
	    x = Math.random() * planetWidth | 0; // |0 to truncate to Int32
	    y = Math.random() * planetHeight | 0;
	    r = 210;//Math.random() * 256 | 0;
	    g = 2  ;//Math.random() * 256 | 0;
	    b = 2  ;//Math.random() * 256 | 0;
	    setPixel( mirroir, x, y, r, g, b, 255); // 255 opaque
	    planetCtx.closePath();
	}
	// les homosapiens
	for (i = 0; i < 10; i++) {
	    x = Math.random() * planetWidth | 0; // |0 to truncate to Int32
	    y = Math.random() * planetHeight | 0;
	    r = 20 ;//Math.random() * 256 | 0;
	    g = 26 ;//Math.random() * 256 | 0;
	    b = 210;//Math.random() * 256 | 0;
	    setPixel( mirroir, x, y, r, g, b, 255); // 255 opaque
	    planetCtx.closePath();
	}
}

function setPixel(imageData, x, y, r, g, b, a) {
	x *= 4 ;
	y *= 4 ;
	x1 = x - (qSize/2*4) ; if ( x1 < 0 ) x1 = 0 ;
	y1 = y - (qSize/2*4) ; if ( y1 < 0 ) y1 = 0 ;
	for ( ii = 0 ; ii < qSize ; ii++ ) {
	  for ( jj = 0 ; jj < qSize ; jj++ ) {
	    index =  Math.floor( x1+(ii*4) + ( (y1+(jj*4)) * imageData.width) ) ;
	    if ( ii==0 && jj == 0 ) { xx=x1 ; yy=y1 ;}
	    //console.log( "setPixel="+index);
	    if ( approveRules( index , r , g , b ) ) {
		    imageData.data[index+0] = r;
		    imageData.data[index+1] = g;
		    imageData.data[index+2] = b;
		    imageData.data[index+3] = a;
	    }
	  }
	}
    planetCtx.putImageData(imageData, 0, 0); // at coords 0,0
	planetCtx.closePath();
}

function approveRules( index , r , g , b ) {
	if ( mirroir.data[index+1] == 250 ) return false ;
	return true ;
}

// Message board
function writeMessage(message) {
    msgCnt+=20 ; 
    if ( msgCnt > 450 ) {
    	msgCnt = 20 ;
    	msgCtx.fillStyle     = 'rgba( 150, 102, 2, 1)';
    	msgCtx.fillRect( 0 , 0 , 200 , 600 );
    	msgCtx.fillStyle     = 'black';
    	msgCtx.closePath();
    }
    msgCtx.fillText(message, 0, msgCnt );
}

// 
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}
//
function stopAnimation() {
	  clearInterval(reproId);
	  clearInterval(extincId);
	  clearInterval(evoId);
}